/* eslint-disable import/prefer-default-export */
import axios from 'axios';

export async function fetchStockPricing() {
  const { data } = await axios.get('https://join.reckon.com/stock-pricing');

  return data;
}
