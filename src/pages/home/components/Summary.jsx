import * as React from 'react';
import PropTypes from 'prop-types';

import './summary.scss';

function Summary({ items }) {
  return (
    <div className="Summary__root">
      <h2 className="title">Summary</h2>
      <div className="Summary__details">
        <table className="table">
          <thead>
            <tr>
              <th>Stock</th>
              <th>Starting</th>
              <th>Lowest</th>
              <th>Highest</th>
              <th>Current</th>
            </tr>
          </thead>
          <tbody>
            {
              items.map((item) => (
                <tr key={item.code}>
                  <td>{item.code}</td>
                  <td>{item.starting}</td>
                  <td>{item.lowest}</td>
                  <td>{item.highest}</td>
                  <td>{item.current}</td>
                </tr>
              ))
            }
          </tbody>
        </table>
      </div>
    </div>
  );
}

Summary.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    code: PropTypes.string,
    starting: PropTypes.number,
    lowest: PropTypes.number,
    highest: PropTypes.number,
    current: PropTypes.number,
  })).isRequired,
};

export default Summary;
