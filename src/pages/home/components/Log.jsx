import * as React from 'react';
import PropTypes from 'prop-types';

import LogRecord from './LogRecord';
import './log.scss';

function Log({ log, pause, resume }) {
  const [isPaused, setPause] = React.useState(false);

  const onClickHandler = () => {
    if (isPaused) {
      setPause(false);
      resume();
    } else {
      setPause(true);
      pause();
    }
  };

  const logRenders = [];

  for (let i = log.length - 1; i >= 0; i -= 1) {
    logRenders.push(
      <LogRecord key={i} updatedAt={log[i].updatedAt} items={log[i].items} />,
    );
  }

  return (
    <div className="Log__root">
      <div className="Log__top-box">
        <h2 className="title">Log</h2>
        <button
          className="button"
          type="button"
          onClick={onClickHandler}
        >
          {
            isPaused ? 'Resume' : 'Pause'
          }
        </button>
      </div>
      <div className="Log__items">
        {
          logRenders
        }
      </div>
    </div>
  );
}

Log.propTypes = {
  log: PropTypes.arrayOf(PropTypes.shape({
    items: PropTypes.arrayOf(PropTypes.shape({
      code: PropTypes.string,
      price: PropTypes.number,
    })).isRequired,
    updatedAt: PropTypes.string.isRequired,
    isPaused: PropTypes.bool.isRequired,
  })),
  pause: PropTypes.func.isRequired,
  resume: PropTypes.func.isRequired,
};

Log.defaultProps = {
  log: [],
};

export default Log;
