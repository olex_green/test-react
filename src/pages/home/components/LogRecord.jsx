import * as React from 'react';
import PropTypes from 'prop-types';

import './logRecord.scss';

function LogRecord({
  updatedAt,
  items,
}) {
  return (
    <div className="LogRecord__root">
      <b>
        Updates for
        {' '}
        {updatedAt}
      </b>
      {
        items.map((item) => (
          <p key={`${item.code}-${updatedAt}`}>
            {item.code}
            : $
            {item.price}
          </p>
        ))
      }
    </div>
  );
}

LogRecord.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    code: PropTypes.string,
    price: PropTypes.number,
  })).isRequired,
  updatedAt: PropTypes.string.isRequired,
};

export default LogRecord;
