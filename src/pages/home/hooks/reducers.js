import {
  ADD_LOG,
  PAUSE,
  RESUME,
} from './actions';

import { calculateSummary } from './helpers';
// eslint-disable-next-line import/prefer-default-export
export function stockPricingReducer(state, action) {
  switch (action.type) {
    case ADD_LOG: {
      const updatedLog = [
        ...state.log,
        {
          items: [
            ...action.payload,
          ],
          updatedAt: new Date().toLocaleString(),
          isPaused: state.isPaused,
        },
      ];

      return {
        ...state,
        log: updatedLog,
        summary: [
          ...calculateSummary(state.summary, action.payload),
        ],
      };
    }
    case PAUSE:
      return {
        ...state,
        isPaused: true,
      };
    case RESUME:
      return {
        ...state,
        isPaused: false,
      };
    default:
      throw new Error('Unknown action');
  }
}
