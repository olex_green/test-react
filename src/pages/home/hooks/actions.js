export const ADD_LOG = 'ADD_LOG';
export const PAUSE = 'PAUSE';
export const RESUME = 'RESUME';

export function createActions(dispatch) {
  return {
    addLog: (payload) => { dispatch({ type: ADD_LOG, payload }); },
    pause: () => { dispatch({ type: PAUSE }); },
    resume: () => { dispatch({ type: RESUME }); },
  };
}
