// eslint-disable-next-line import/prefer-default-export
export function calculateSummary(summaries = [], newLog) {
  if (newLog.length === 0) {
    return [];
  }

  const newSummaries = newLog.map((l) => {
    const sum = summaries.find((s) => s.code === l.code);

    if (sum) {
      return {
        ...sum,
        lowest: sum.lowest > l.price ? l.price : sum.lowest,
        highest: sum.highest < l.price ? l.price : sum.highest,
        current: l.price,
      };
    }

    return {
      code: l.code,
      starting: l.price,
      lowest: l.price,
      highest: l.price,
      current: l.price,
    };
  });

  return newSummaries;
}
