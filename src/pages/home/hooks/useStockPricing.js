import { useEffect, useReducer } from 'react';

import { fetchStockPricing } from '../api';
import { stockPricingReducer } from './reducers';
import { createActions } from './actions';

const INITIAL_STATE = {
  log: [],
  summary: [],
  isPaused: false,
};

const FETCHING_INTERVAL_MS = 2000;

export default function useStockPricing() {
  const [state, dispatch] = useReducer(stockPricingReducer, INITIAL_STATE);
  const {
    addLog,
    pause,
    resume,
  } = createActions(dispatch);

  useEffect(() => {
    let intervalID;
    const asyncCall = async () => {
      intervalID = setInterval(async () => {
        try {
          const log = await fetchStockPricing();
          addLog(log);
        } catch (err) {
          // eslint-disable-next-line no-console
          console.log('fetchStockPricing is failed: ', err.message);
        }
      }, FETCHING_INTERVAL_MS);
    };

    asyncCall();

    return () => {
      if (intervalID) {
        clearInterval(intervalID);
      }
    };
  });

  const actions = {
    pause,
    resume,
  };

  return [state, actions];
}
