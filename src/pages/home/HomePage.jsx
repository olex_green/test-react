import * as React from 'react';

import Log from './components/Log';
import Summary from './components/Summary';
import useStockPricing from './hooks/useStockPricing';
import './styles.scss';

export default function HomePage() {
  const [state, actions] = useStockPricing();

  const filteredLog = state.log.filter((l) => !l.isPaused);

  return (
    <div className="HomePage__root">
      <Log log={filteredLog} pause={actions.pause} resume={actions.resume} />
      <Summary items={state.summary} />
    </div>
  );
}
