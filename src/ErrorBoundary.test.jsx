import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { stub } from 'sinon';

import ErrorBoundary from './ErrorBoundary';
import * as logs from './utils/logs';

function setup(specProps) {
  const defaultProps = {
  };

  const props = {
    ...defaultProps,
    ...specProps,
  };
  // eslint-disable-next-line react/jsx-props-no-spreading
  const enzymeWrapper = shallow(<ErrorBoundary {...props} />);

  return {
    props,
    enzymeWrapper,
  };
}

describe('ErrorBoundary', () => {
  it('should render children', () => {
    const { enzymeWrapper } = setup({ children: <div className="test">test</div> });
    expect(enzymeWrapper.first().is('div.test')).to.equal(true);
  });

  it('set state hasError to true. Should contain h1 with expected text', () => {
    const { enzymeWrapper } = setup({ children: <div className="test">test</div> });
    enzymeWrapper.instance().setState({ hasError: true });
    expect(enzymeWrapper.find('h1').first().text()).to.equal('Something went wrong.');
  });

  it('set state hasError to true. Should contain h1 with expected text', () => {
    const { enzymeWrapper } = setup({ children: <div className="test">test</div> });
    enzymeWrapper.instance().setState({ hasError: true });
    expect(enzymeWrapper.find('article').first().text()).to.equal('Please reload the current page');
  });

  it('componentDidCatch should call logs.error', () => {
    const errorStub = stub(logs, 'error');
    const { enzymeWrapper } = setup({ children: <div className="test">test</div> });
    enzymeWrapper.instance().componentDidCatch();

    expect(errorStub.calledOnce).to.equal(true);
    errorStub.restore();
  });

  it('getDerivedStateFromError should return expected state', () => {
    const expected = { hasError: true };

    expect(ErrorBoundary.getDerivedStateFromError()).to.deep.equal(expected);
  });
});
