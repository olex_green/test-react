import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import Routes from './Routes';
import App from './App';

function setup(specProps) {
  const defaultProps = {
  };

  const props = {
    ...defaultProps,
    ...specProps,
  };
  // eslint-disable-next-line react/jsx-props-no-spreading
  const enzymeWrapper = shallow(<App {...props} />);

  return {
    props,
    enzymeWrapper,
  };
}

describe('App', () => {
  it('should has Routes', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(Routes).length).to.equal(1);
  });
});
