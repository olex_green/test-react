import * as React from 'react';

import HomePage from './pages/home/HomePage';

import './polyfills';

function App() {
  return (
    <HomePage />
  );
}

export default App;
