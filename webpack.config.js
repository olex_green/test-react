// eslint-disable-next-line import/no-extraneous-dependencies
const path = require('path');

const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ResourceHintWebpackPlugin = require('resource-hints-webpack-plugin');

module.exports = {
  context: path.resolve(__dirname, './src'),
  target: 'web',
  mode: 'development',
  entry: path.resolve(__dirname, './src/index.jsx'),
  devtool: 'cheap-module-eval-source-map',
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: [
      path.join(__dirname, 'node_modules'),
      path.join(__dirname, 'src'),
    ],
  },
  devServer: {
    historyApiFallback: true,
    clientLogLevel: 'warning',
    compress: true,
    hot: true,
    inline: true,
    port: process.env.PORT || 9898,
    host: 'localhost',
    disableHostCheck: true,
    quiet: false,
    noInfo: false,
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: [path.join(__dirname, 'node_modules')],
        loader: 'babel-loader',
        include: [path.join(__dirname, 'src')],
        options: {
          babelrc: true,
        },
      },
      {
        test: /\.s?[ac]ss$/,
        exclude: [path.join(__dirname, 'node_modules')],
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: process.env.NODE_ENV === 'develop',
            },
          },
          'css-loader',
          'sass-loader',
          'postcss-loader',
        ],
      },
      {
        test: /\.s?[ac]ss$/,
        include: [path.join(__dirname, 'node_modules')],
        use: ['style-loader', 'css-loader', 'sass-loader', 'postcss-loader'],
      },
      {
        test: /\.(jpe?g|png|gif|webm|webp)$/i,
        loader: 'file-loader',
        options: {
          name() {
            if (process.env.NODE_ENV === 'development') {
              return '[path][name].[ext]';
            }

            return '[path][name].[ext]'; // [hash]
          },
          outputPath: 'images/',
        },
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack', 'url-loader'],
      },
      // Font Definitions
      {
        test: /\.(woff(2)?|ttf|eot|otf)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/',
            },
          },
        ],
      },
      {
        test: /\.md$/i,
        use: 'raw-loader',
      },
    ],
  },
  optimization: {
    minimize: false,
  },
  watchOptions: {
    ignored: /node_modules/,
  },
  plugins: [
    // new webpack.HotModuleReplacementPlugin(),
    new ReactRefreshWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: './public/index.ejs',
      minify: {
        removeComments: true,
        collapseWhitespace: true,
      },
      inject: true,
      alwaysWriteToDisk: true,
    }),
    new ScriptExtHtmlWebpackPlugin(),
    new HtmlWebpackHarddiskPlugin({
      outputPath: path.resolve(__dirname, 'dist'),
    }),
    new ResourceHintWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[name].css',
    }),
  ],
};
